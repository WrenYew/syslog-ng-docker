 #!/usr/bin/python3
import sys
import syslog
import logging
import logging.handlers


# https://docs.python.org/3/howto/logging-cookbook.html#sending-and-receiving-logging-events-across-a-network
rootLogger = logging.getLogger('')
rootLogger.setLevel(logging.NOTSET)
# socketHandler = logging.handlers.SocketHandler('localhost', logging.handlers.DEFAULT_TCP_LOGGING_PORT)
# socketHandler = logging.handlers.SocketHandler('localhost', 601)
socketHandler = logging.handlers.SocketHandler('localhost', 1514)
# don't bother with a formatter, since a socket handler sends the event as
# an unformatted pickle
rootLogger.addHandler(socketHandler)

# Now, we can log to the root logger, or any other logger. First the root...
rootLogger.info('Jackdaws love my big sphinx of quartz.')
rootLogger.debug('Jackdaws love my big sphinx of quartz.')

# Now, define a couple of other loggers which might represent areas in your
# application:

logger1 = logging.getLogger('myapp.area1')
logger2 = logging.getLogger('myapp.area2')
logger1.addHandler(socketHandler)
logger2.addHandler(socketHandler)

logger1.debug('Quick zephyrs blow, vexing daft Jim.')
logger1.info('How quickly daft jumping zebras vex.')
logger2.warning('Jail zesty vixen who grabbed pay from quack.')
logger2.error('The five boxing wizards jump quickly.')


# http://www.whiteboardcoder.com/2017/03/syslog-ng-install-and-configure.html
#Set up Logger
# logger = logging.getLogger("10x13")
# logger.setLevel(logging.DEBUG)
# handler = logging.handlers.SysLogHandler(address='/dev/klog',facility=syslog.LOG_LOCAL0)
# handler = logging.StreamHandler(sys.stdout)
# log_format = logging.Formatter('%(asctime)s, %(message)s')
# handler.setFormatter(log_format)
# logger.addHandler(handler)

# logger.info("Info Log Message")
# logger.debug("Debug Log Message")
# logger.error("Error log Message")
# logger.error(logging.handlers.DEFAULT_TCP_LOGGING_PORT)
